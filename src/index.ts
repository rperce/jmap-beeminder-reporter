import { JmapSession } from './adapters/jmap.js'
import { BeeminderAdapter } from './adapters/beeminder.js'

function die(msg: string) {
  console.warn(msg);
  process.exit(1);
}

type UnixTime = number;
type Seconds = number;
function timeDelta(timestamp: UnixTime): Seconds {
  const now = Math.floor(Date.now() / 1000);
  return now - timestamp;
}

(async () => {
  const envVarBlank = (name: string): boolean => !process.env[name] || process.env[name] === '';
  if (envVarBlank('JMAP_USER') !== envVarBlank('JMAP_PASS')) {
    die(`Error: JMAP_USER and JMAP_PASS must be both set or both unset, but user is (${process.env.JMAP_USER}) and pass is (${process.env.JMAP_PASS})`)
  }

  if (envVarBlank('JMAP_USER') === envVarBlank('JMAP_TOKEN')) {
    die('Error: specify either (JMAP_USER and JMAP_PASS) or JMAP_TOKEN.')
  }

  if (envVarBlank('BEEMINDER_TOKEN')) {
    die('Error: BEEMINDER_TOKEN is blank.')
  }

  const jmap = await new JmapSession('api.fastmail.com').authenticate({
    user: process.env.JMAP_USER,
    pass: process.env.JMAP_PASS,
    token: process.env.JMAP_TOKEN,
  })
  const beeminder = new BeeminderAdapter(process.env.BEEMINDER_TOKEN)

  try {
    const inboxCount = parseInt(await jmap.mailCount(), 10)
    const lastDatapoint = await beeminder.getLastDatapoint('inbox-zero')

    const addDatapoint = () => beeminder.addDatapoint('inbox-zero', inboxCount)

    if (inboxCount !== lastDatapoint.value) {
      await addDatapoint()
      console.log('Submitted datapoint with new inbox count', inboxCount)
    } else if (timeDelta(lastDatapoint.timestamp) > 6 * 60 * 60) {
      await addDatapoint()
      console.log('Last datapoint is more than 6h old; resubmitted', inboxCount)
    } else {
      console.log(`Last datapoint already ${inboxCount}: took no action`)
    }
  } catch (e) {
    console.warn(`Error: ${e.message}`)
  }
})()
