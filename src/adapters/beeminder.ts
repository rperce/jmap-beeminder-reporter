import got from 'got'

export class BeeminderApiException extends Error { }

export class BeeminderAdapter {
  apiToken: string
  constructor(apiToken: string) {
    this.apiToken = apiToken
  }

  async getLastDatapoint(slug: string) {
    try {
      const response = (await got({
        url: `https://beeminder.com/api/v1/users/me/goals/${slug}/datapoints.json`,
        searchParams: {
          auth_token: this.apiToken,
          count: 1,
        },
      }).json()) as any

      return {
        value: response[0].value,
        timestamp: response[0].timestamp,
      }
    } catch (e) {
      throw new BeeminderApiException(
        `Failure getting last datapoint from beeminder goal ${slug}: ${e.message}`
      )
    }
  }

  async addDatapoint(slug: string, value: number) {
    try {
      await got({
        method: 'POST',
        url: `https://beeminder.com/api/v1/users/me/goals/${slug}/datapoints.json`,
        searchParams: {
          auth_token: this.apiToken,
        },
        json: {
          value,
          comment: `Created by jmap-beeminder-reporter at ${new Date().toISOString()}`,
        },
      })
    } catch (e) {
      throw new BeeminderApiException(
        `Failure adding datapoint to beeminder goal ${slug}: ${e.message}`
      )
    }
  }
}

new BeeminderAdapter(process.env.BEEMINDER_TOKEN).getLastDatapoint('inbox-zero')
