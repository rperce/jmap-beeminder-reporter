import got, { Got } from 'got'

export class JmapApiException extends Error { }

type BasicAuth = { user: string, pass: string }
type BearerAuth = { token: string }
const authorizationHeader = (auth: BasicAuth | BearerAuth): string => {
  if ('pass' in auth && auth.pass) {
    const token = Buffer.from(`${auth.user}:${auth.pass}`).toString('base64');
    return `Basic ${token}`
  } else if ('token' in auth && auth.token) {
    return `Bearer ${auth.token}`;
  }
}


export class JmapSession {
  hostname: string
  constructor(hostname: string) {
    this.hostname = hostname
  }

  async authenticate(
    auth: BasicAuth | BearerAuth
  ): Promise<AuthenticatedJmapSession> {
    try {
      const authHeader = authorizationHeader(auth)
      const response = (await got({
        url: `https://${this.hostname}/.well-known/jmap`,
        headers: {
          'Content-Type': 'application/json',
          Authorization: authHeader,
        },
      }).json()) as any

      const { apiUrl, primaryAccounts } = response
      const accountId = primaryAccounts['urn:ietf:params:jmap:mail']

      return new AuthenticatedJmapSession(authHeader, apiUrl, accountId)
    } catch (e) {
      console.error(e);
      const user = 'user' in auth && auth.user ? `user [${auth.user}]` : 'user'
      throw new JmapApiException(`Failure authenticating ${user}`, e)
    }
  }
}

export class AuthenticatedJmapSession {
  authHeader: string
  apiUrl: string
  accountId: string
  got: Got
  constructor(authHeader: string, apiUrl: string, accountId: string) {
    this.authHeader = authHeader
    this.apiUrl = apiUrl
    this.accountId = accountId
  }

  async mailCount(): Promise<string> {
    try {
      const response = (await got({
        url: this.apiUrl,
        method: 'POST',
        headers: {
          Authorization: this.authHeader,
          'Content-Type': 'application/json',
        },
        json: {
          using: ['urn:ietf:params:jmap:core', 'urn:ietf:params:jmap:mail'],
          methodCalls: [
            [
              'Mailbox/query',
              {
                accountId: this.accountId,
                filter: { role: 'inbox', hasAnyRole: true },
              },
              'inbox',
            ],
            [
              'Mailbox/get',
              {
                accountId: this.accountId,
                properties: ['totalEmails'],
                '#ids': {
                  resultOf: 'inbox',
                  name: 'Mailbox/query',
                  path: '/ids/*',
                },
              },
              'mailcount',
            ],
          ],
        },
      }).json()) as any

      return response.methodResponses[1][1].list[0].totalEmails
    } catch (e) {
      throw new JmapApiException(`Failure getting inbox count`)
    }
  }
}
