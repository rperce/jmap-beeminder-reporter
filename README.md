# jmap-beeminder-reporter

[Beeminder](https://beeminder.com) has a Gmail integration for creating inbox-zero style
goals. Unfortunately, they don't have integrations with any other providers.

I use Fastmail, which provides API access over [JMAP](https://jmap.io/), so I've written
this small script and a systemd service to automatically update my own [inbox-zero
goal](https://beeminder.com/rperce/inbox-zero).

## Installation

This runs as a systemd timer once an hour, at every `*:58:00`. It is intended to be run
on a persistent server, or at least persistent-ish.

If you're using Arch Linux or another distro that uses `pacman`, download the
"Transpiled JS with PKGBUILD" artifact from the [latest release], extract it, and make
the package:
```
tar xzf jmap-beeminder-reporter.tar.gz
cd jmap-beeminder-reporter
makepkg -sri
```

On other systems, you'll need to manually move the `.cjs` into your `PATH`,  the
`.service` and `.timer` to somewhere that systemd can see, and the `.conf` to
`/etc/conf.d/` (or, if you edit the path in the `.service` file, wherever you want).
That conf file will contain your JMAP password and beeminder token in plaintext, so
ensure it's chmodded `600` and owned by root.

### Building from source

The goal slug `inbox-zero` and the JMAP api url
`https://betajmap.fastmail.com/.well-known/jmap` are hardcoded. If you want a different
slug or have your email on something else that supports JMAP (there doesn't appear to be
much yet at time of writing), install a node version that matches the `engines` key in
`package.json`, then edit those undesired strings in `src/index.ts` and run 
```
yarn install
./Taskfile build
./Taskfile package
```
then proceed with the installation instructions above.

## Config

Edit `/etc/conf.d/jmap-beeminder-reporter` and insert your beeminder token and either
your JMAP username and password (for basic auth) or a bearer token (for Fastmail, use an
API token from Settings -> Account -> Password and Security -> API tokens -> Add).

## Enabling

Once installed, run
```
sudo systemctl enable --now jmap-beeminder-reporter.timer
```

[latest release]: https://gitlab.com/rperce/jmap-beeminder-reporter/-/releases/permalink/latest/downloads/jmap-beeminder-reporter.tar.gz
