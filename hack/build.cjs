require('esbuild').build({
  entryPoints: ['src/index.ts'],
  bundle: true,
  outfile: 'dist/jmap-beeminder-reporter.cjs',
  platform: 'node',
  target: 'node16',
  banner: { js: '#!/usr/bin/env node' },
})
