#!/usr/bin/env sh
pkgname=jmap-beeminder-reporter

pjs() { jq -r "$1" package.json; }
md5() { md5sum "$1" | cut -f1 -d\ ; }

cat <<EOF
# Maintainer: $(pjs .author)
pkgname=$pkgname
pkgver=$(pjs .version)
pkgrel=1
epoch=
pkgdesc="$(pjs .description)"
arch=('any')
url="$(pjs .repository)"
license=('$(pjs .license)')
depends=('nodejs$(pjs .engines.node)')
backup=('etc/conf.d/jmap-beeminder-reporter.conf')
source=("\$pkgname.cjs"
        "\$pkgname.service"
        "\$pkgname.timer"
        "\$pkgname.conf"
)
md5sums=("$(md5 dist/$pkgname.cjs)"
         "$(md5 dist/$pkgname.service)"
         "$(md5 dist/$pkgname.timer)"
         "$(md5 dist/$pkgname.conf)"
)

package() {
  install -Dm755 "\${srcdir}/\${pkgname}.cjs" "\${pkgdir}/usr/bin/\${pkgname}.cjs"
  install -Dm644 "\${srcdir}/\${pkgname}.service" "\${pkgdir}/usr/lib/systemd/system/\${pkgname}.service"
  install -Dm644 "\${srcdir}/\${pkgname}.timer" "\${pkgdir}/usr/lib/systemd/system/\${pkgname}.timer"
  install -Dm600 "\${srcdir}/\${pkgname}.conf" "\${pkgdir}/etc/conf.d/\${pkgname}.conf"
}
EOF
